<?php

namespace Drupal\trinion_cart\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\trinion_cart\Controller\CartController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a cart block.
 *
 * @Block(
 *   id = "trinion_cart_cart_block",
 *   admin_label = @Translation("Cart block"),
 *   category = @Translation("TrinionStore")
 * )
 */
class CartBlock extends BlockBase implements ContainerFactoryPluginInterface {

  protected $trinionStoreCart;

  /**
   * Constructs a new CartBlock instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartController $trinion_cart_cart) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->trinionStoreCart = $trinion_cart_cart;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('trinion_cart.cart')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $cart_data = $this->trinionStoreCart->getCartData();
    $build['#cache']['max-age'] = 0;
    $build['content'] = [
      '#theme' => 'cart_block',
      '#cart_data' => $cart_data,
      '#operation' => 'view-item'
    ];
    return $build;
  }

}
