<?php

namespace Drupal\trinion_cart\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Cart delete field field handler.
 *
 * @ViewsField("cart_delete_field")
 */
class CartDeleteField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    return [
      '#theme' => 'cart_delete_field',
      '#nid' => $values->_entity->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }
}
