<?php

namespace Drupal\trinion_cart\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Provides Cart skidka field handler.
 *
 * @ViewsField("cart_skidka_field")
 */
class CartSkidkaField extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $skidka = (float)$values->_entity->get('field_tp_skidka_summa')->getString();
    return [
      '#markup' => $skidka == 0 ? '' : $skidka,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
  }
}
