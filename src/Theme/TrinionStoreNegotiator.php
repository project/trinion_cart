<?php

namespace Drupal\trinion_cart\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\user\Entity\User;

/**
 * Определение темы для store
 */
class TrinionStoreNegotiator implements ThemeNegotiatorInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route_name = $route_match->getRouteName();
    if ($route_name == 'entity.node.canonical') {
      $user = \Drupal::currentUser();
      if ($user->isAuthenticated()) {
        $user = User::load($user->id());
        if ($user && $user->get('field_tb_erp_user')->getString()) {
          return FALSE;
        }
      }
      if (in_array($route_match->getParameter('node')->bundle(), \Drupal::service('trinion_tp.helper')->getProductBundles()))
        return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return \Drupal::config('system.theme')->get('default');
  }
}
