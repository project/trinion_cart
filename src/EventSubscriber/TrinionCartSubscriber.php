<?php

namespace Drupal\trinion_cart\EventSubscriber;

use Drupal\Component\Utility\Random;
use Drupal\trinion_cart\Event\PaymentEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Trinion cart event subscriber.
 */
class TrinionCartSubscriber implements EventSubscriberInterface {

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Response event.
   */
  public function onPaymentSuccess(PaymentEvent $event) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      PaymentEvent::PAYMENT_SUCCESS => ['onPaymentSuccess'],
    ];
  }

}
