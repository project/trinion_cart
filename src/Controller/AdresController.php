<?php

namespace Drupal\trinion_cart\Controller;

use Drupal\node\Entity\Node;

/**
 * Trinion cart Payment service.
 */
class AdresController {
  /**
   * Получения списка адресов пользователя
   */
  public function getUserAdreses($uid) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'adres')
      ->condition('field_tc_polzovatel', $uid);
    $res = $query->accessCheck()->execute();
    return $res ? Node::loadMultiple($res) : [];
  }

  /**
   * Создание адреса
   */
  public function createAdres($uid, $data) {
    $adres_data = [
      'type' => 'adres',
      'title' => $this->generateAdresName($data),
      'uid' => $uid,
      'status' => 1,
      'field_tc_polzovatel' => $uid,
    ];
    $adres_data += $data;
    $node = Node::create($adres_data);
    $node->save();
    return $node;
  }

  /**
   * Изменение адреса
   */
  public function updateAdres($adres_id, $data) {
    $node = Node::load($adres_id);
    foreach ($data as $field_name => $value) {
      if ($field_name == 'field_tc_polzovatel')
        continue;
      $node->{$field_name} = $value;
    }
    $node->title = $this->generateAdresName($data);
    $node->save();
    return $node;
  }

  /**
   * Формирование названия адреса
   */
  public function generateAdresName($data) {
    return $this->generateFIOString($data);
  }

  /**
   * Формирование ФИО для Заказа клиента из полей Адреса
   */
  public function generateFIOString($data) {
    $name = [];
    if ($data['field_tc_familiya'])
      $name[] = trim($data['field_tc_familiya']);
    if ($data['field_tc_imya'])
      $name[] = trim($data['field_tc_imya']);
    return $name ? implode(' ', $name) : 'Без имени';
  }

  /**
   * Формирование полного адреса для Заказа клиента из полей Адреса
   */
  public function generateFullAdresString($data) {
    $adres = [];
    if (!empty($data['field_tc_indeks']))
      $adres[] = trim($data['field_tc_indeks']);
    if (!empty($data['field_tc_adres']))
      $adres[] = trim($data['field_tc_adres']);
    if (!empty($data['field_tc_podezd']))
      $adres[] = 'подъезд ' . trim($data['field_tc_podezd']);
    if (!empty($data['field_tc_etazh']))
      $adres[] = 'этаж ' . trim($data['field_tc_etazh']);
    if (!empty($data['field_tc_kvartira']))
      $adres[] = 'кв. ' . trim($data['field_tc_kvartira']);
    return trim(implode(', ', $adres));
  }
}
