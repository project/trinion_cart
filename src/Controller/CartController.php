<?php

namespace Drupal\trinion_cart\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\SettingsCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\node\Entity\Node;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Returns responses for Trinion store routes.
 */
class CartController extends ControllerBase {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * @var \Symfony\Component\HttpFoundation\Session\Session
   */
  protected $session;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\node\Entity\Node
   */
  protected $cart;

  protected $user_id = 0;

  protected $sid;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection, AccountProxy $current_user, Session $session, EntityTypeManager $entity_type_manager) {
    $this->connection = $connection;
    $this->currentUser = $current_user;
    $this->session = $session;
    $this->entityTypeManager = $entity_type_manager;

    $this->loadCurrentCart();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('current_user'),
      $container->get('session'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Add to cart
   */
  public function cartButton($nid) {
    $count = \Drupal::request()->get('count');
    if (is_null($count))
      $count = 1;
    $this->addToCart($nid, '+', $count);
    $product = Node::load($nid);
    $response = new AjaxResponse();
    if ($product->hasField('field_tp_bistraya_pokupka') && $product->get('field_tp_bistraya_pokupka')->getString()) {
      $response->addCommand(new RedirectCommand('/cart/checkout'));
    }
    else {
      $cart_data = $this->getCartData();
      $cart_block = [
        '#theme' => 'cart_block',
        '#cart_data' => $cart_data,
      ];
      $cart_add_info = [
        '#theme' => 'cart_add_info',
        '#cart_data' => $cart_data,
      ];

      $response->addCommand(new SettingsCommand(['cart_data' => $cart_data], TRUE));
      $response->addCommand(new ReplaceCommand('.cart_block', $cart_block));
      $response->addCommand(new HtmlCommand('#tmodal-wrapper', $cart_add_info));
    }
    return $response;
  }

  /**
   * Add to cart
   */
  public function fastCheckout($nid) {
    $count = \Drupal::request()->get('count');
    if (is_null($count))
      $count = 1;

    $cart_data = $this->getCartData();

    foreach ($cart_data['items'] as $item) {
      $this->deleteFromCart($item['cart_item_id']);
    }

    $this->addToCart($nid, '+', $count);

    $form = \Drupal::formBuilder()->getForm('Drupal\trinion_cart\Form\FastCheckoutForm');

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('#fast-checkout-wrapper', $form));
    return $response;
  }

  /**
   * Удаление элемента из корзины
   */
  public function deleteItem($nid) {
    $response = new AjaxResponse();
    $this->deleteFromCart($nid);
    $cart_data = $this->getCartData();
    $cart_block = [
      '#theme' => 'cart_block',
      '#cart_data' => $cart_data,
      '#operation' => 'delete-item'
    ];

    $view = Views::getView('tcart');
    $display_id = 'page_1';
    $args = [];
    $view->setDisplay($display_id);
    trinion_cart_views_pre_view($view, $display_id, $args);

    $cart = $view->render();

    $response->addCommand(new SettingsCommand(['cart_data' => $cart_data], TRUE));
    $response->addCommand(new ReplaceCommand('.cart_block', $cart_block));
    $response->addCommand(new ReplaceCommand('.tcart', $cart));
    return $response;
  }

  /**
   * Очистка корзины
   */
  public function clearCart() {
    $response = new AjaxResponse();
    $cart_data = $this->getCartData();

    foreach ($cart_data['items'] as $item) {
      $this->deleteFromCart($item['cart_item_id']);
    }

    $cart_data = $this->getCartData();
    $cart_block = [
      '#theme' => 'cart_block',
      '#cart_data' => $cart_data,
      '#operation' => 'delete-item'
    ];

    $view = Views::getView('tcart');
    $display_id = 'page_1';
    $args = [];
    $view->setDisplay($display_id);
    trinion_cart_views_pre_view($view, $display_id, $args);

    $cart = $view->render();

    $response->addCommand(new SettingsCommand(['cart_data' => $cart_data], TRUE));
    $response->addCommand(new ReplaceCommand('.cart_block', $cart_block));
    $response->addCommand(new ReplaceCommand('.tcart', $cart));
    return $response;
  }

  /**
   * Запуск сессии
   */
  public function startSession() {
    if (!$this->session->isStarted()) {
      $this->session->set('session_started', TRUE);
      $this->session->save();
    }
  }

  /**
   * Получение текущей корзины пользователя
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function loadCurrentCart() {
    $storage = $this->entityTypeManager->getStorage('node');
    $this->user_id = $this->currentUser->id();
    $query = $storage->getQuery()
      ->condition('type', 'zakaz_klienta');
    if ($this->user_id) {
      $query->condition('uid', $this->user_id);
    }
    else {
      $this->startSession();
      $this->sid = $this->session->getId();
      $query->condition('field_tc_sid', $this->sid);
    }
    $query->condition('field_tc_status_korziny', 'cart');
    $cart = $query->accessCheck()->execute();

    $this->cart = $cart ? $storage->load(reset($cart)) : FALSE;
  }

  public function addToCart($nid, $op = '+', $qty = 1) {
    $cart_item_added = FALSE;
    $harakteristika = \Drupal::request()->get('harakteristika');
    $storage = $this->entityTypeManager->getStorage('node');
    if ($this->cart) {
      // Плюсую товар к количеству, если уже есть
      foreach ($this->cart->get('field_tp_stroki') as $item) {
        $cart_item = $item->entity;
        if ($cart_item && $cart_item->get('field_tp_tovar')->getString() == $nid) {
          if ($harakteristika && $harakteristika != $cart_item->get('field_tp_kharakteristika_tovara')->getString())
            continue;
          $current_qty = $cart_item->get('field_tp_kolichestvo')->getString();
          if ($op == '+')
            $cart_item->field_tp_kolichestvo = $current_qty + $qty;
          elseif ($op == '-')
            $cart_item->field_tp_kolichestvo = $current_qty - $qty;
          elseif ($op == '=')
            $cart_item->field_tp_kolichestvo = $qty;
          $cart_item->field_tp_itogo = $cart_item->get('field_tp_kolichestvo')->getString() * $cart_item->get('field_tp_cena')->getString();
          $cart_item->save();
          $cart_item_added = TRUE;
        }
      }
      $this->cart->save();
    }
    else {
      if (empty($_SERVER['HTTP_REFERER']))
        return;
      $now = DrupalDateTime::createFromTimestamp(time());
      $now->setTimezone(new \DateTimeZone('UTC'));
      $responsible = \Drupal::config('trinion_cart.settings')->get('default_responsible');
      $cart_data = [
        'title' => 'Cart',
        'type' => 'zakaz_klienta',
        'status' => 1,
        'uid' => $this->user_id,
        'field_tp_data' => $now->format('Y-m-d\TH:i:s'),
        'field_tp_zakaz_dlya' => \Drupal::config('trinion_cart.settings')->get('company_nid'),
        'field_tp_otvetstvennyy' => $responsible ? $responsible : 1,
      ];
      if (!$this->user_id) {
        $cart_data['field_tc_sid'] = $this->sid;
      }
      $cart = $storage->create($cart_data);
      $cart->save();
      $cart->set('title', \Drupal::service('trinion_tp.helper')->getNextDocumentNumber('zakaz_klienta'));
      $cart->save();
      $this->cart = $cart;
    }
    // Если до сих пор cart_item не обновлен, значит его нет, создаю и цепляю к корзине
    if (!$cart_item_added) {
      $storage = $this->entityTypeManager->getStorage('node');
      $product = $storage->load($nid);
      $cena = \Drupal::service('trinion_tp.helper')->getPolzovatelskayaCenaTovara($product, $harakteristika);
      $cart_item_data = [
        'type' => 'tp_stroka_dokumenta_uit',
        'status' => 1,
        'uid' => $this->user_id,
        'field_tp_kolichestvo' => $qty,
        'title' => $product->label(),
        'field_tp_artikul' => $product->get('field_tp_artikul')->getString(),
        'field_tp_tovar' => $nid,
        'field_tp_cena' => $cena,
      ];
      if ($nds = $product->get('field_tp_nds')->getString())
        $cart_item_data['field_tp_nds'] = $nds;
      if ($edinica = $product->get('field_tp_edinica_izmereniya')->getString())
        $cart_item_data['field_tp_edinica_izmereniya'] = $edinica;
      if ($harakteristika)
        $cart_item_data['field_tp_kharakteristika_tovara'] = $harakteristika;
      $cart_item_data['field_tp_itogo'] = $cena;
      $cart_item = $storage->create($cart_item_data);
      $cart_item->save();
      $this->cart->field_tp_stroki[] = $cart_item->id();
      $this->cart->save();
    }

    $this->reapplyCoupons();
  }

  /**
   * Данные корзины
   * @return array
   */
  public function getCartData() {
    $data = ['total_price' => 0, 'total_qty' => 0, 'total_skidka' => 0, 'total_nds' => 0, 'items' => [], ];
    if ($this->cart) {
      foreach ($this->cart->get('field_tp_stroki') as $cart_item) {
        $cart_item = $cart_item->entity;
        if (is_null($cart_item))
          continue;
        $price = $cart_item->get('field_tp_cena')->getString();
        $skidka = (float)$cart_item->get('field_tp_skidka_summa')->getString();
        $qty = $cart_item->get('field_tp_kolichestvo')->getString();
        $nds = floatval($cart_item->get('field_tp_nds')->getString());
        $data['total_nds'] += ($price * $qty - $skidka) / (100 + $nds) * $nds;
        $data['total_price'] += $price * $qty - $skidka;
        $data['total_skidka'] += $skidka;
        $data['total_qty'] += $qty;
        $data['cart'] = $this->cart;
        $data['items'][] = [
          'name' => $cart_item->label(),
          'sku' => $cart_item->get('field_tp_artikul')->getString(),
          'price' => $price,
          'qty' => $qty,
          'cart_item_id' => $cart_item->id(),
          'product' => $cart_item->get('field_tp_tovar')->entity,
          'cart_item' => $cart_item,
        ];
      }
    }
    $data['total_nds'] = round($data['total_nds'], 2);
    return $data;
  }

  /**
   * Поля заказа
   * @return array
   */
  public function getCartFields() {
    $fields = [];
    if ($this->cart) {
      $exclude = ['field_tc_sposob_oplaty', 'field_tc_sid', 'field_tp_adres_dostavki', 'field_tp_fio'];
      foreach ($this->cart->getFields() as $field_name =>$field_data) {
        if (strpos($field_name, 'field_') === 0 && !in_array($field_name, $exclude)) {
          /** @var \Drupal\field\Entity\FieldConfig $data_definition */
          $data_definition = $field_data->getDataDefinition();
          $field = [
            '#title' => $data_definition->getLabel(),
            '#required' => $data_definition->isRequired(),
          ];
          switch ($data_definition->getType()) {
            case 'string':
              $field['#type'] = 'textfield';
              $field['#default_value'] = $field_data->getString();
              $fields[$field_name] = $field;
              break;
            case 'email':
              $field['#type'] = 'email';
              $field['#default_value'] = $field_data->getString();
              $fields[$field_name] = $field;
              break;
            case 'string_long':
              $field['#type'] = 'textarea';
              $field['#default_value'] = $field_data->getString();
              $fields[$field_name] = $field;
              break;
          }
        }
      }
    }
    return $fields;
  }

  /**
   * Проверка наличия товара в корзине
   * @param $nid
   */
  public function checkInCart($nid) {
    if ($this->cart) {
      foreach ($this->cart->get('field_tp_stroki') as $cart_item) {
        $cart_item = $cart_item->entity;
        if ($cart_item && $cart_item->get('field_tp_tovar')->getString() == $nid) {
          return TRUE;
        }
      }
    }
    return FALSE;
  }

  public function cartRecalculate() {
    $response = new AjaxResponse();
    $data = \Drupal::request()->get('data');
    if ($data && is_array($data)) {
      foreach ($data as $item) {
        $this->addToCart($item['nid'], '=', $item['qty']);
      }
    }
    else {
      $this->reapplyCoupons();
    }

    $cart_data = $this->getCartData();
    $cart_block = [
      '#theme' => 'cart_block',
      '#cart_data' => $cart_data,
    ];

    $view = Views::getView('tcart');
    $display_id = 'page_1';
    $args = [];
    $view->setDisplay($display_id);
    trinion_cart_views_pre_view($view, $display_id, $args);

    $cart = $view->render();

    $coupon_form = [
      '#theme' => 'coupon_form',
      '#cart' => $cart_data['cart'],
    ];

    $view = Views::getView('tcart');
    $display_id = 'block_1';
    $args = [];
    $view->setDisplay($display_id);
    trinion_cart_views_pre_view($view, $display_id, $args);

    $cart_block_checkout = $view->render();

    $response->addCommand(new SettingsCommand(['cart_data' => $cart_data], TRUE));
    $response->addCommand(new ReplaceCommand('.cart_block', $cart_block));
    $response->addCommand(new ReplaceCommand('.tcart', $cart));
    $response->addCommand(new ReplaceCommand('.tcart-cart-block', $cart_block_checkout));
    $response->addCommand(new ReplaceCommand('#coupon_form', $coupon_form));
    return $response;
  }

  /**
   * Удаление товара из корзины
   * @param $nid
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteFromCart($nid) {
    if ($this->cart) {
      /** @var \Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem $item */
      foreach ($this->cart->get('field_tp_stroki') as $key => $item) {
        if ($item->entity->id() == $nid) {
          foreach ($this->cart->get('field_tc_primenennye_kupony') as $coup_key => $coup) {
            $tovar = $coup->entity->get('field_tp_tovar')->getString();
            if (empty($tovar) || $item->entity->get('field_tp_tovar')->getString() == $coup->entity->get('field_tp_tovar')->getString()) {
              $harakteristika = $coup->entity->get('field_tp_kharakteristika_tovara')->getString();
              if (empty($harakteristika) || $harakteristika == $item->entity->get('field_tp_kharakteristika_tovara')->getString()) {
                unset($this->cart->get('field_tc_primenennye_kupony')[$coup_key]);
              }
            }
          }
          $item->entity->delete();
          unset($this->cart->get('field_tp_stroki')[$key]);
        }
      }
      $this->cart->save();
    }
  }

  public function successPage() {
    return [
      '#theme' => 'success_page'
    ];
  }

  /**
   * Получение купона по названию
   * @param $name
   * @return \Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCouponByName($name) {
    $storage = $this->entityTypeManager->getStorage('node');
    $query = $storage->getQuery()
      ->condition('type', 'kupon')
      ->condition('title', $name);
    $res = $query->accessCheck()->execute();
    if ($res) {
      $nid = reset($res);
      return $storage->load($nid);
    }
  }

  public function reapplyCoupons() {
    $tempstore = \Drupal::service('tempstore.private');
    $coupons_storage = $tempstore->get('coupons');
    $coupons_state = $coupons_storage->get('coupons_state');
    foreach ($this->cart->get('field_tc_primenennye_kupony') as $coup) {
      if ($coupons_state[$coup->entity->id()])
        $this->applyCoupon($coup->entity->label());
    }
  }

  /**
   * Применение купона к корзине
   * @param $coupon
   * @return AjaxResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function applyCoupon($coupon) {
    $exist= FALSE;

    foreach ($this->cart->get('field_tc_primenennye_kupony') as $coup) {
      if ($coup->entity->label() == $coupon) {
        $exist = TRUE;
        break;
      }
    }
    $coup = $this->getCouponByName($coupon);
    if ($coup) {
      if (!$exist) {
        $this->cart->field_tc_primenennye_kupony[] = $coup->id();
      }
      foreach ($this->cart->get('field_tp_stroki') as $key => $item) {
        $tovar = $coup->get('field_tp_tovar')->getString();
        if (empty($tovar) || $item->entity->get('field_tp_tovar')->getString() == $tovar) {
          $harakteristika = $coup->get('field_tp_kharakteristika_tovara')->getString();
          if (empty($harakteristika) || $harakteristika == $item->entity->get('field_tp_kharakteristika_tovara')->getString()) {
            $kupon_skidka = $coup->get('field_tp_cena')->getString();
            $item->entity->field_tp_skidka_summa = $kupon_skidka;
            $item->entity->field_tp_itogo = $item->entity->get('field_tp_cena')->getString() * $item->entity->get('field_tp_kolichestvo')->getString() - $kupon_skidka;
            $item->entity->save();
          }
        }
      }
      $this->cart->save();
    }
    $response = new AjaxResponse();
    return $response;
  }

  /**
   * Удаление купона
   */
  public function removeCoupon($coupon, $full_remove = TRUE) {
    $coupons = [];

    $coup = $this->entityTypeManager->getStorage('node')->load($coupon);
    foreach ($this->cart->get('field_tp_stroki') as $key => $item) {
      $tovar = $coup->get('field_tp_tovar')->getString();
      if (empty($tovar) || $item->entity->get('field_tp_tovar')->getString() == $tovar) {
        $harakteristika = $coup->get('field_tp_kharakteristika_tovara')->getString();
        if (empty($harakteristika) || $harakteristika == $item->entity->get('field_tp_kharakteristika_tovara')->getString()) {
          $item->entity->field_tp_skidka_summa = 0;
//          $item->entity->field_tp_cena = $item->entity->get('field_tp_cena')->getString() + $coup->get('field_tp_cena')->getString();
          $item->entity->field_tp_itogo = $item->entity->get('field_tp_cena')->getString() * $item->entity->get('field_tp_kolichestvo')->getString();
          $item->entity->save();
        }
      }
    }

    if ($full_remove) {
      foreach ($this->cart->get('field_tc_primenennye_kupony') as $coup) {
        $id = $coup->entity->id();
        if ($id != $coupon)
          $coupons[] = $id;
      }
      $this->cart->field_tc_primenennye_kupony = $coupons;
    }
    $this->cart->save();
    $response = new AjaxResponse();
    return $response;
  }

  /**
   * Добавления купона
   */
  public function addCoupon($coupon) {
    $coup = $this->getCouponByName($coupon);
    if ($coup) {
      $this->applyCoupon($coupon);
      $this->activateCoupon($coup->id(), 1, FALSE);
    }
    $response = new AjaxResponse();
    return $response;
  }

  /**
   * Активация/деактивация купона
   * @param $coupon
   */
  public function activateCoupon($coupon, $op, $recalc = TRUE) {
    $tempstore = \Drupal::service('tempstore.private');
    $coupons_storage = $tempstore->get('coupons');
    $coupons_state = $coupons_storage->get('coupons_state');
    if (is_null($coupons_state))
      $coupons_state = [];
    $coupons_state[$coupon] = $op;
    $coupons_storage->set('coupons_state', $coupons_state);

    if ($recalc) {
      if ($op)
        $this->applyCoupon($coupon);
      else
        $this->removeCoupon($coupon, FALSE);
    }

    $response = new AjaxResponse();
    return $response;
  }
}

