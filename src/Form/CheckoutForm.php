<?php

namespace Drupal\trinion_cart\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Provides a Trinion store form.
 */
class CheckoutForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_cart_checkout';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $adres_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', 'adres');
    $form = \Drupal::service('trinion_cart.cart')->getCartFields();
    $form['#attached']['library'][] = 'trinion_cart/checkout';
    $user = \Drupal::currentUser();
    $cart_data = \Drupal::service('trinion_cart.cart')->getCartData();
    $hide_address = TRUE;
    foreach ($cart_data['items'] as $item) {
      if ($item['product']->get('field_tp_tsifrovoy_tovar')->getString() == 0)
        $hide_address = FALSE;
    }

    if (!$hide_address) {
      $form['adreses'] = [
        '#tree' => TRUE,
      ];
      $is_auth = $user->isAuthenticated();
      if ($is_auth) {
        $user = User::load($user->id());

        $adreses = \Drupal::service('trinion_cart.adres')->getUserAdreses($user->id());
        $first = TRUE;
        foreach ($adreses as $adres) {
          $adres_id = $adres->id();
          $key = 'adres_' . $adres_id;
          $form['adreses'][$key]['adres'] = [
            '#type' => 'checkbox',
            '#title' => $adres->label(),
            '#return_value' => $adres_id,
            '#default_value' => $first ? $adres_id : NULL,
            '#attributes' => ['class' => ['adres-radio-selector']],
          ];
          $form['adreses'][$key]['edit'] = [
            '#type' => '#markup',
            '#markup' => '<span class="data-adres-edit" data-adres-edit="' . $adres_id . '">Изменить</span>',
          ];
          foreach ($adres_fields as $name => $field) {
            if ($name != 'field_tc_polzovatel' && strpos($name, 'field_tc') === 0) {
              $form['adreses'][$key][$name] = [
                '#type' => 'textfield',
                '#title' => $field->label(),
                '#element_validate' => $field->isRequired() ? ['::requiredValidate'] : [],
                '#default_value' => $adres->get($name)->getString(),
              ];
            }
          }
          $first = FALSE;
        }

        $form['adreses']['adres_new']['adres'] = [
          '#type' => 'checkbox',
          '#title' => 'Новый адрес',
          '#return_value' => 'new',
          '#default_value' => empty($adreses) ? 'new' : NULL,
          '#attributes' => ['class' => ['adres-radio-selector']],
        ];

        if (isset($form['field_tp_fio'])) {
          $name = [];
          if ($familiya = $user->get('field_tb_familiya')->getString())
            $name[] = $familiya;
          if ($imya = $user->get('field_tb_imya')->getString())
            $name[] = $imya;
          $name = implode(' ', $name);
          if ($name) {
            $form['field_tp_fio']['#value'] = $name;
            $form['field_tp_fio']['#disabled'] = TRUE;
          }
        }
      }
      foreach ($adres_fields as $name => $field) {
        if ($name != 'field_tc_polzovatel' && strpos($name, 'field_tc') === 0) {
          $form['adreses']['adres_new'][$name] = [
            '#type' => 'textfield',
            '#title' => $field->label(),
            '#element_validate' => $field->isRequired() ? ['::requiredValidate'] : [],
          ];
        }
      }

      $form['adreses']['adres_new']['save_new'] = [
        '#type' => 'checkbox',
        '#title' => 'Сохранить адрес',
      ];
      if (!$is_auth) {
        $form['adreses']['adres_new']['adres']['#type'] = 'hidden';
        $form['adreses']['adres_new']['adres']['#value'] = 'new';
        $form['adreses']['adres_new']['save_new']['#type'] = 'hidden';
        $form['adreses']['adres_new']['save_new']['#value'] = '1';
      }
    }
    if ($user->getEmail()) {
      $form['field_tp_email']['#value'] = $user->getEmail();
      $form['field_tp_email']['#disabled'] = TRUE;
    }

    $block = [
      '#theme' => 'cart_total_checkout',
      '#total_price' => $cart_data['total_price'],
      '#total_qty' => $cart_data['total_qty'],
      '#total_skidka' => $cart_data['total_skidka'] ?? 0,
      '#total_nds' => $cart_data['total_nds'] ?? 0,
      '#cart_data' => $cart_data,
    ];
    $summary = \Drupal::service('renderer')->render($block);
    $form['summary'] = [
      '#type' => 'markup',
      '#markup' => $summary,
    ];

    $form['payment_method'] = [
      '#type' => 'container',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Оплатить',
    ];

    return $form;
  }

  public function requiredValidate(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $adres_check_parents = $form['#parents'];
    $adres_check_parents[2] = 'adres';
    $adres_check = NestedArray::getValue($values, $adres_check_parents);
    if ($adres_check) {
      $value = NestedArray::getValue($values, $form['#parents']);
      if (trim($value) == '')
        $form_state->setErrorByName('', t('@name field is required.', ['@name' => $form['#title']]));
    }
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (isset($form['payment_method'])) {
      $payment_method = \Drupal::request()->request->get('payment_method');
      if (is_null($payment_method)) {
        $form_state->setErrorByName('', 'Выберите способ оплаты');
      }
    }

    $current_user = \Drupal::currentUser();
    if ($current_user->isAnonymous()) {
      if (!filter_var($form_state->getValue('field_tp_email'), FILTER_VALIDATE_EMAIL))
        $form_state->setError($form['field_tp_email'], 'Укажите корректный email');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $cart = \Drupal::service('trinion_cart.cart')->getCartData();
    if ($cart['items']) {
      $order = $cart['cart'];
      $values = $form_state->getValues();
      foreach ($values as $field_name => $value) {
        $order->{$field_name} = $value;
      }
      $order->field_tc_status_korziny = 'completed';
      $current_user = \Drupal::currentUser();
      if ($current_user->isAnonymous()) {
        if ($email = $order->get('field_tp_email')->getString()) {
          $query = \Drupal::entityQuery('user')
            ->condition('mail', $email);
          $res = $query->accessCheck()->execute();
          if ($res) {
            $uid = reset($res);
          }
          else {
            $user = \Drupal\user\Entity\User::create();
            $random = new Random();
            $pass = $random->word(8);
            $user->setPassword($pass);
            $user->enforceIsNew();
            $user->setEmail($email);
            $user->status = 1;
            $user->setUsername($email);
            $user->save();
            $uid = $user->id();
            \Drupal::service('trinion_main.mails')->uvedomleniePolzovateliaORegistraciiPosleZakaza($order, $email, $pass);
          }
          $order->uid = $uid;
        }
      }
      else {
        $uid = $current_user->id();
      }

      if (isset($values['adreses'])) {
        foreach ($values['adreses'] as $adres) {
          if ($adres['adres']) {
            if ($adres['adres'] == 'new') {
              unset($adres['adres']);
              if ($adres['save_new'])
                \Drupal::service('trinion_cart.adres')->createAdres($uid, $adres);
            } elseif (!empty($adres['adres'])) {
              \Drupal::service('trinion_cart.adres')->updateAdres($adres['adres'], $adres);
            }
            $order->field_tp_adres_dostavki = \Drupal::service('trinion_cart.adres')->generateFullAdresString($adres);
            $order->field_tp_fio = \Drupal::service('trinion_cart.adres')->generateFIOString($adres);
          }
        }
      }

      $order->save();
      $form_state->set('order', $order);
      $form_state->set('cart', $cart);

      $tempstore = \Drupal::service('tempstore.private');
      $coupons_storage = $tempstore->get('coupons');
      $coupons_storage->set('coupons_state', []);

      if (\Drupal::request()->request->get('payment_method') == NULL) {
        $form_state->setRedirect('trinion_cart.default_success');
      }
    }
  }
}
