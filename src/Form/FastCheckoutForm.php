<?php

namespace Drupal\trinion_cart\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Random;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Provides a Trinion store form.
 */
class FastCheckoutForm extends CheckoutForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_cart_fast_checkout';
  }
}
