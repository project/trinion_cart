(function ($, Drupal) {
  Drupal.behaviors.tcheckout = {
    attach: function (context, settings) {
      $(".adres-radio-selector").click(function(){
        var adr_id = $(this).val();
        $(".adres-radio-selector").prop("checked", "");

        $(this).prop("checked", "checked");
        $("[class*='form-item--adreses-adres-']:not(.form-type--checkbox), .form-item--adreses-adres-new-save-new").hide();
        if (adr_id == 'new')
          $("[class*='form-item--adreses-adres-" + adr_id + "'], .form-item--adreses-adres-new-save-new").show();

        $(".data-adres-edit").hide();
        $(".data-adres-edit[data-adres-edit='" + adr_id + "']").show();
      });
      $(".data-adres-edit").click(function(e){
        var adr_id = $(this).data('adres-edit');
        $("[class*='form-item--adreses-adres-" + adr_id + "']").show();
      });
      $(".adres-radio-selector:checked").trigger("click");
    }
  };
})(jQuery, Drupal);
