(function ($, Drupal) {
  Drupal.behaviors.tcart = {
    attach: function (context, settings) {
      $(".cart-recalculate").click(function(e){
        e.preventDefault();
        recalculateRequest();
      });
      $(".tcart input.qty-field").keyup(function(){
        recalculateRequest();
      });
      $(".tcart .qty-plus").unbind("click").click(function(e){
        e.preventDefault();
        if (!$(this).hasClass("in-process")) {
          $(this).addClass("in-process");
          var nid = $(this).data("nid");
          var input = $(this).closest(".tcart").find(`input.qty-field[data-nid='${nid}']`);
          input.val(parseInt(input.val()) + 1);
          recalculateRequest();
          $(this).removeClass("in-process");
        }
      });
      $(".tcart .qty-minus").unbind("click").click(function(e){
        e.preventDefault();
        if (!$(this).hasClass("in-process")) {
          $(this).addClass("in-process");
          var nid = $(this).data("nid");
          var input = $(this).closest(".tcart").find(`input.qty-field[data-nid='${nid}']`);
          var cnt = parseInt(input.val());
          if (cnt > 0) {
            input.val(cnt - 1);
            recalculateRequest();
          }
        }
      });
      $(".tcart input.qty-field").change(function(){
        recalculateRequest();
      });
      $("#coupon-input").keypress(function(e){
        if(e.which == 13) {
          $("#coupon-apply").click();
        }
      });
      $("#coupon-apply").click(function(e){
        e.preventDefault();
        var coupon = $("#coupon-input").val();
        if (coupon != '') {
          Drupal.ajax({
            url: "/cart/coupon/apply/" + coupon
          }).execute().done(function () {
            recalculateRequest();
          });
        }
      });

      $(".remove-coupon").click(function(e){
        e.preventDefault();
        Drupal.ajax({
          url: "/cart/coupon/remove/" + $(this).data('coupon-id')
        }).execute().done(function () {
          recalculateRequest();
        });
      });

      $(".activate-coupon").click(function(e){
        var op = $(this).prop('checked') ? 1 : 0;
        Drupal.ajax({
          url: "/cart/coupon/activate/" + $(this).data('coupon-id') + '/' + op
        }).execute().done(function () {
          recalculateRequest();
        });
      });

      $(".clear-cart").click(function(e){
        e.preventDefault();
        $(".tcart").html('');
        Drupal.ajax({
          url: "/cart/clear"
        }).execute().done(function () {
          recalculateRequest();
        });
      });

      function recalculateRequest() {
        var data = [];
        $(".tcart input.qty-field").each(function(){
          data.push({
            nid: $(this).data("nid"),
            qty: $(this).val()
          });
        });
        Drupal.ajax({
          url: "/cart/recalculate",
          submit: {
            data: data,
          },
          complete: function() {
            $(".tcart .qty-plus, .tcart .qty-minus").removeClass("in-process");
          }
        }).execute();
      }
    }
  };
})(jQuery, Drupal);
