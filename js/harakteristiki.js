(function ($, Drupal) {
  Drupal.behaviors.harakteristiki = {
    attach: function (context, settings) {
      once('harakteristiki', 'body', context).forEach(function (element) {
        $("#harakteristiki").change(function(){
          var harakteristika = $(this).val();
          $("#harakteristika-price").html(settings.ceni_harakteristik[harakteristika]);
          $(".add_to_cart").each(function(){
            var url = $(this).data("href");
            url = url.replace(/harakteristika=\d+/g, 'harakteristika=' + harakteristika);
            $(this).data('href', url);
          });
        });
        $(".add_to_cart").click(function(e){
          var url = $(this).data('href');
          var qty_id_selector = $(this).data('qty-id-selector');
          if (qty_id_selector) {
            if ($("#harakteristiki").length)
              url = url + '&';
            else
              url = url + '?';
            url = url + 'count=' + $("#" + qty_id_selector).val();
          }
          e.preventDefault();
          Drupal.ajax({
            url: url
          }).execute();
        })
      });
    }
  };
})(jQuery, Drupal);
